# README #

This was originally forked from https://github.com/emilybache/SupermarketReceipt-Refactoring-Kata. So far, only the refactoring exercise has been made.

The main take away from the refactoring exercise was moving away from an enum type for SpecialOffer, to an abstract Offer, that can be extended with the different types of offers. Another improvement is fixing the feature envy between Receipt and ShoppingCart with regard to the receipt handling.
 