using SupermarketReceipt.Offers;
using Xunit;

namespace SupermarketReceipt.Test
{
    public class SupermarketTest
    {
        [Fact, Trait("ProductUnit", "Each")]
        public void ProductsWithEachUnit_WithNoSpecialOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var toothbrush = new Product("toothbrush", ProductUnit.Each);
            catalog.AddProduct(toothbrush, 0.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(toothbrush, 3);

            var teller = new Teller(catalog);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(3 * 0.99, receipt.GetTotalPrice());
            Assert.Empty(receipt.GetDiscounts());
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(toothbrush, receiptItem.Product);
                Assert.Equal(0.99, receiptItem.Price);
                Assert.Equal(3 * 0.99, receiptItem.TotalPrice);
                Assert.Equal(3, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Each")]
        public void ProductsWithEachUnit_With10PercentDiscount()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var rice = new Product("rice bag", ProductUnit.Each);
            catalog.AddProduct(rice, 2.49);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(rice, 3);

            var teller = new Teller(catalog);

            var tenPercentDiscountOffer = new PercentageDiscountOffer(rice, 10);
            teller.AddSpecialOffer(tenPercentDiscountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(6.723, receipt.GetTotalPrice(), 3);
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(0.747, discount.DiscountAmount, 3); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(rice, receiptItem.Product);
                Assert.Equal(2.49, receiptItem.Price);
                Assert.Equal(3 * 2.49, receiptItem.TotalPrice);
                Assert.Equal(3, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Each")]
        public void ProductsWithEachUnit_WithThreeForTwoOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var toothbrush = new Product("toothbrush", ProductUnit.Each);
            catalog.AddProduct(toothbrush, 0.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(toothbrush, 7);

            var teller = new Teller(catalog);

            var threeForTwoOffer = new QuantityForPriceOfQuantityOffer(toothbrush, 3, 2);
            teller.AddSpecialOffer(threeForTwoOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(4.95, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(1.98, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(toothbrush, receiptItem.Product);
                Assert.Equal(0.99, receiptItem.Price);
                Assert.Equal(7 * 0.99, receiptItem.TotalPrice);
                Assert.Equal(7, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Each")]
        public void ProductsWithEachUnit_WithTwoForAmountOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var cherryTomatoes = new Product("box of cherry tomatoes", ProductUnit.Each);
            catalog.AddProduct(cherryTomatoes, 0.69);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(cherryTomatoes, 5);

            var teller = new Teller(catalog);

            var twoForAmountOffer = new QuantityForAmountOffer(cherryTomatoes, 2, 0.99);
            teller.AddSpecialOffer(twoForAmountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(2.67, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(0.78, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(cherryTomatoes, receiptItem.Product);
                Assert.Equal(0.69, receiptItem.Price);
                Assert.Equal(5 * 0.69, receiptItem.TotalPrice);
                Assert.Equal(5, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Each")]
        public void ProductsWithEachUnit_WithFiveForAmountOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var toothpaste = new Product("toothpaste", ProductUnit.Each);
            catalog.AddProduct(toothpaste, 1.79);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(toothpaste, 11);

            var teller = new Teller(catalog);

            var fiveForAmountOffer = new QuantityForAmountOffer(toothpaste, 5, 7);
            teller.AddSpecialOffer(fiveForAmountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(15.79, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(3.9, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(toothpaste, receiptItem.Product);
                Assert.Equal(1.79, receiptItem.Price);
                Assert.Equal(11 * 1.79, receiptItem.TotalPrice);
                Assert.Equal(11, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Kilo")]
        public void ProductsWithKiloUnit_WithNoSpecialOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var apples = new Product("apples", ProductUnit.Kilo);
            catalog.AddProduct(apples, 1.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(apples, 2.5);

            var teller = new Teller(catalog);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(2.5 * 1.99, receipt.GetTotalPrice());
            Assert.Empty(receipt.GetDiscounts());
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(apples, receiptItem.Product);
                Assert.Equal(1.99, receiptItem.Price);
                Assert.Equal(2.5 * 1.99, receiptItem.TotalPrice);
                Assert.Equal(2.5, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Kilo")]
        public void ProductsWithKiloUnit_With10PercentDiscount()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var apples = new Product("apples", ProductUnit.Kilo);
            catalog.AddProduct(apples, 1.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(apples, 2.5);

            var teller = new Teller(catalog);

            var tenPercentDiscountOffer = new PercentageDiscountOffer(apples, 10);
            teller.AddSpecialOffer(tenPercentDiscountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(4.4775, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(0.4975, discount.DiscountAmount); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(apples, receiptItem.Product);
                Assert.Equal(1.99, receiptItem.Price);
                Assert.Equal(2.5 * 1.99, receiptItem.TotalPrice);
                Assert.Equal(2.5, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Kilo")]
        public void ProductsWithKiloUnit_WithThreeForTwoOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var apples = new Product("apples", ProductUnit.Kilo);
            catalog.AddProduct(apples, 1.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(apples, 7);

            var teller = new Teller(catalog);

            var threeForTwoOffer = new QuantityForPriceOfQuantityOffer(apples, 3, 2);
            teller.AddSpecialOffer(threeForTwoOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(9.95, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(3.98, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(apples, receiptItem.Product);
                Assert.Equal(1.99, receiptItem.Price);
                Assert.Equal(7 * 1.99, receiptItem.TotalPrice);
                Assert.Equal(7, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Kilo")]
        public void ProductsWithKiloUnit_WithTwoForAmountOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var apples = new Product("apples", ProductUnit.Kilo);
            catalog.AddProduct(apples, 1.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(apples, 5);

            var teller = new Teller(catalog);

            var twoForAmountOffer = new QuantityForAmountOffer(apples, 2, 2.5);
            teller.AddSpecialOffer(twoForAmountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(6.99, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(2.96, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(apples, receiptItem.Product);
                Assert.Equal(1.99, receiptItem.Price);
                Assert.Equal(5 * 1.99, receiptItem.TotalPrice);
                Assert.Equal(5, receiptItem.Quantity);
            });
        }

        [Fact, Trait("ProductUnit", "Kilo")]
        public void ProductsWithKiloUnit_WithFiveForAmountOffer()
        {
            // ARRANGE
            ISupermarketCatalog catalog = new FakeCatalog();

            var apples = new Product("apples", ProductUnit.Kilo);
            catalog.AddProduct(apples, 1.99);

            var cart = new ShoppingCart();
            cart.AddItemQuantity(apples, 11);

            var teller = new Teller(catalog);

            var fiveForAmountOffer = new QuantityForAmountOffer(apples, 5, 6);
            teller.AddSpecialOffer(fiveForAmountOffer);

            // ACT
            var receipt = teller.ChecksOutArticlesFrom(cart);

            // ASSERT
            Assert.Equal(13.99, receipt.GetTotalPrice());
            Assert.Collection(receipt.GetDiscounts(), discount => { Assert.Equal(7.9, discount.DiscountAmount, 2); });
            Assert.Collection(receipt.GetItems(), receiptItem =>
            {
                Assert.Equal(apples, receiptItem.Product);
                Assert.Equal(1.99, receiptItem.Price);
                Assert.Equal(11 * 1.99, receiptItem.TotalPrice);
                Assert.Equal(11, receiptItem.Quantity);
            });
        }
    }
}