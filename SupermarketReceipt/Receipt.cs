using System.Collections.Generic;

namespace SupermarketReceipt
{
    public class Receipt
    {
        private readonly List<Discount> discounts = new List<Discount>();
        private readonly List<ReceiptItem> items = new List<ReceiptItem>();
        private double totalPrice = 0.0;

        public double GetTotalPrice()
        {
            return totalPrice;
        }

        public void AddProduct(Product p, double quantity, double price)
        {
            var item = new ReceiptItem(p, quantity, price);
            items.Add(item);
            totalPrice += item.TotalPrice;
        }

        public List<ReceiptItem> GetItems()
        {
            return new List<ReceiptItem>(items);
        }

        public void AddDiscount(Discount discount)
        {
            discounts.Add(discount);
            totalPrice -= discount.DiscountAmount;
        }

        public List<Discount> GetDiscounts()
        {
            return new List<Discount>(discounts);
        }
    }
}