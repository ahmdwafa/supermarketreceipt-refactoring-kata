using SupermarketReceipt.Offers;
using System.Collections.Generic;

namespace SupermarketReceipt
{
    public class ShoppingCart
    {
        private readonly List<ProductQuantity> items = new List<ProductQuantity>();
        private readonly Dictionary<Product, double> productQuantities = new Dictionary<Product, double>();

        public List<ProductQuantity> GetItems()
        {
            return new List<ProductQuantity>(items);
        }

        public void AddItem(Product product)
        {
            AddItemQuantity(product, 1.0);
        }

        public void AddItemQuantity(Product product, double quantity)
        {
            items.Add(new ProductQuantity(product, quantity));
            if (productQuantities.ContainsKey(product))
            {
                var newAmount = productQuantities[product] + quantity;
                productQuantities[product] = newAmount;
            }
            else
            {
                productQuantities.Add(product, quantity);
            }
        }

        public List<Discount> GetItemsDiscounts(Dictionary<Product, Offer> offers, ISupermarketCatalog catalog)
        {
            var discounts = new List<Discount>();

            foreach (var p in productQuantities.Keys)
            {
                var quantity = productQuantities[p];
                if (offers.ContainsKey(p))
                {
                    var offer = offers[p];
                    Discount discount = offer.GetDiscount(quantity, catalog);

                    if (discount != null)
                        discounts.Add(discount);
                }
            }

            return discounts;
        }
    }
}