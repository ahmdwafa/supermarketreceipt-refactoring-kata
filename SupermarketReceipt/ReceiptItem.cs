﻿namespace SupermarketReceipt
{
    public class ReceiptItem
    {
        public ReceiptItem(Product p, double quantity, double price)
        {
            Product = p;
            Quantity = quantity;
            Price = price;
        }

        public Product Product { get; }
        public double Quantity { get; }
        public double Price { get; }
        public double TotalPrice { get { return Price * Quantity; } }
    }
}