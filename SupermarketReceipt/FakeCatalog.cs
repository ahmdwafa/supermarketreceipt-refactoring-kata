using System.Collections.Generic;

namespace SupermarketReceipt
{
    public class FakeCatalog : ISupermarketCatalog
    {
        private readonly IDictionary<string, double> prices = new Dictionary<string, double>();
        private readonly IDictionary<string, Product> products = new Dictionary<string, Product>();

        public void AddProduct(Product product, double price)
        {
            products.Add(product.Name, product);
            prices.Add(product.Name, price);
        }

        public double GetUnitPrice(Product p)
        {
            return prices[p.Name];
        }
    }
}