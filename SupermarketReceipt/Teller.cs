using SupermarketReceipt.Offers;
using System.Collections.Generic;

namespace SupermarketReceipt
{
    public class Teller
    {
        private readonly ISupermarketCatalog catalog;
        private readonly Dictionary<Product, Offer> offers = new Dictionary<Product, Offer>();

        public Teller(ISupermarketCatalog catalog)
        {
            this.catalog = catalog;
        }

        public void AddSpecialOffer(Offer offer)
        {
            var product = offer.GetProduct();
            offers[product] = offer;
        }

        public Receipt ChecksOutArticlesFrom(ShoppingCart theCart)
        {
            var receipt = new Receipt();

            var productQuantities = theCart.GetItems();
            foreach (var pq in productQuantities)
            {
                var unitPrice = catalog.GetUnitPrice(pq.Product);
                receipt.AddProduct(pq.Product, pq.Quantity, unitPrice);
            }

            var discounts = theCart.GetItemsDiscounts(offers, catalog);
            foreach (var discount in discounts) receipt.AddDiscount(discount);

            return receipt;
        }
    }
}