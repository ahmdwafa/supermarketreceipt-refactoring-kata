﻿namespace SupermarketReceipt.Offers
{
    public class QuantityForPriceOfQuantityOffer : Offer
    {
        public QuantityForPriceOfQuantityOffer(Product product, int quantity, int quantityOfPrice)
            : base(product)
        {
            Quantity = quantity;
            QuantityOfPrice = quantityOfPrice;
        }

        public int Quantity { get; }
        public int QuantityOfPrice { get; }

        public override Discount GetDiscount(double soldQuantity, ISupermarketCatalog catalog) => GetDiscount((int)soldQuantity, catalog);

        public Discount GetDiscount(int soldQuantity, ISupermarketCatalog catalog)
        {
            int discountedUnitsOfQuantity = Quantity > soldQuantity ? 0 : soldQuantity / Quantity;
            if (discountedUnitsOfQuantity > 0)
            {
                var unitPrice = catalog.GetUnitPrice(product);
                double totalBeforeDiscount = soldQuantity * unitPrice;

                double offerPrice = unitPrice * QuantityOfPrice;

                int normalUnits = soldQuantity - (discountedUnitsOfQuantity * Quantity);

                double totalAfterDiscount = (unitPrice * normalUnits) + (discountedUnitsOfQuantity * offerPrice);
                double discountAmount = totalBeforeDiscount - totalAfterDiscount;

                return new Discount(product, ToString(), discountAmount);
            }

            return null;
        }

        public override string ToString()
        {
            return $"{Quantity} for price of {QuantityOfPrice} offer";
        }
    }
}