﻿namespace SupermarketReceipt.Offers
{
    public class PercentageDiscountOffer : Offer
    {
        public PercentageDiscountOffer(Product product, double percentageOff)
            : base(product)
        {
            PercentageOff = percentageOff;
        }

        public double PercentageOff { get; }

        public override Discount GetDiscount(double quantity, ISupermarketCatalog catalog)
        {
            var unitPrice = catalog.GetUnitPrice(product);
            var total = unitPrice * quantity;
            var discountAmount = (PercentageOff / 100) * total;

            return new Discount(product, ToString(), discountAmount);
        }

        public override string ToString()
        {
            return $"{PercentageOff}% off.";
        }
    }
}
