﻿namespace SupermarketReceipt.Offers
{
    public class QuantityForAmountOffer : Offer
    {
        public QuantityForAmountOffer(Product product, int quantity, double amount)
            : base(product)
        {
            Quantity = quantity;
            Amount = amount;
        }

        public int Quantity { get; }
        public double Amount { get; }

        public override Discount GetDiscount(double soldQuantity, ISupermarketCatalog catalog) => GetDiscount((int)soldQuantity, catalog);

        public Discount GetDiscount(int soldQuantity, ISupermarketCatalog catalog)
        {
            int discountedUnitsOfQuantity = Quantity > soldQuantity ? 0 : soldQuantity / Quantity;
            if (discountedUnitsOfQuantity > 0)
            {
                var normalUnitPrice = catalog.GetUnitPrice(product);
                double totalBeforeDiscount = soldQuantity * normalUnitPrice;

                int normalUnits = soldQuantity - (discountedUnitsOfQuantity * Quantity);

                double totalAfterDiscount = (normalUnitPrice * normalUnits) + (discountedUnitsOfQuantity * Amount);
                double discountAmount = totalBeforeDiscount - totalAfterDiscount;

                return new Discount(product, ToString(), discountAmount);
            }

            return null;
        }

        public override string ToString()
        {
            return $"{Quantity} for {Amount} offer";
        }
    }
}