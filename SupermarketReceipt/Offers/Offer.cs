namespace SupermarketReceipt.Offers
{
    public abstract class Offer
    {
        protected readonly Product product;

        protected Offer(Product product)
        {
            this.product = product;
        }

        public Product GetProduct()
        {
            return new Product(product.Name, product.Unit);
        }

        public abstract Discount GetDiscount(double soldQuantity, ISupermarketCatalog catalog);

        public abstract override string ToString();
    }
}